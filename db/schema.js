const { gql } = require('apollo-server');

//schema
const typeDefs = gql`
  type Usuario {
    id: ID
    nombre: String
    apellido: String
    email: String
    creado: String
  }

  type Token {
    token: String
  }

  type Curso {
    id: ID
    nombre: String
    existencia: Int
    fechaInicio: String
    fechaTermino: String
    horaInicio: String
    horaTermino: String
    maestro: String
    aula: Int
    creado: String
  }

  type Club {
    id: ID
    nombre: String
    clave: String
    existencia: Int
    dia: String
    fechaInicio: String
    fechaTermino: String
    horaInicio: String
    horaTermino: String
    maestro: Maestro
    aula: String
    creado: String
  }

  type ClubMasculino {
    id: ID
    nombre: String
    clave: String
    existencia: Int
    dia: String
    fechaInicio: String
    fechaTermino: String
    horaInicio: String
    horaTermino: String
    maestro: Maestro
    aula: String
    creado: String
  }

  type ClubFemenino {
    id: ID
    nombre: String
    clave: String
    existencia: Int
    dia: String
    fechaInicio: String
    fechaTermino: String
    horaInicio: String
    horaTermino: String
    maestro: Maestro
    aula: String
    creado: String
  }

  type Taller {
    id: ID
    nombre: String
    clave: String
    existencia: Int
    dia: String
    fechaInicio: String
    fechaTermino: String
    horaInicio: String
    horaTermino: String
    maestro: Maestro
    aula: String
    creado: String
  }

  type Estudiante {
    id: ID
    nombre: String
    paterno: String
    materno: String
    email: String
    nacimiento: String
    semestre: String
    genero: String
    curp: String
    sangre: String
    alergias: String
    padecimientos: String
    matricula: String
    creado: String
    grupo: ID
  }
  type Maestro {
    id: ID
    nombre: String
    paterno: String
    materno: String
    email: String
    telefono: String
    celular: String
    curp: String
    sangre: String
    alergias: String
    padecimientos: String
    creado: String
    grupo: ID
  }

  type MasEstudiante {
    id: ID
    nombre: String
    paterno: String
    materno: String
    email: String
    nacimiento: String
    semestre: String
    genero: String
    curp: String
    sangre: String
    alergias: String
    padecimientos: String
    creado: String
    grupo: ID
  }

  type FemEstudiante {
    id: ID
    nombre: String
    paterno: String
    materno: String
    email: String
    nacimiento: String
    semestre: String
    genero: String
    curp: String
    sangre: String
    alergias: String
    padecimientos: String
    creado: String
    grupo: ID
  }
  type TopEstudiante {
    total: Float!
    estudiante: [Estudiante]
  }
  type TopGrupo {
    total: Float!
    grupo: [Usuario]
  }
  type Pedido {
    id: ID
    pedido: [PedidoGrupo]
    total: Float
    estudiante: Estudiante
    grupo: ID
    fecha: String
    estado: EstadoPedido
  }

  type PedidoGrupo {
    id: ID
    cantidad: Int
  }

  type Historial {
    id: ID
    pedido: [PedidoGrupo]
    total: Int
    estudiante: ID
    grupo: ID
    fecha: String
    estado: EstadoPedido
  }

  input UsuarioInput {
    nombre: String!
    apellido: String!
    email: String!
    password: String!
  }

  input AutenticarInput {
    email: String!
    password: String!
  }

  input CursoInput {
    nombre: String!
    existencia: Int!
    fechaInicio: String!
    fechaTermino: String!
    horaInicio: String!
    horaTermino: String!
    maestro: String!
    aula: Int!
  }

  input ClubInput {
    nombre: String!
    clave: String!
    existencia: Int!
    dia: String!
    fechaInicio: String!
    fechaTermino: String!
    horaInicio: String!
    horaTermino: String!
    maestro: ID!
    aula: String!
  }

  input ClubMasculinoInput {
    nombre: String!
    clave: String!
    existencia: Int!
    dia: String!
    fechaInicio: String!
    fechaTermino: String!
    horaInicio: String!
    horaTermino: String!
    maestro: ID!
    aula: String!
  }

  input ClubFemeninoInput {
    nombre: String!
    existencia: Int!
    clave: String!
    dia: String!
    fechaInicio: String!
    fechaTermino: String!
    horaInicio: String!
    horaTermino: String!
    maestro: ID!
    aula: String!
  }

  input TallerInput {
    nombre: String!
    existencia: Int!
    clave: String!
    dia: String!
    fechaInicio: String!
    fechaTermino: String!
    horaInicio: String!
    horaTermino: String!
    maestro: ID!
    aula: String!
  }

  input EstudianteInput {
    nombre: String!
    paterno: String!
    materno: String!
    email: String!
    nacimiento: String!
    semestre: String!
    genero: String!
    curp: String!
    sangre: String!
    alergias: String!
    padecimientos: String!
    matricula: String!
  }
  input ActualizarGpoEstInput {
    grupo: ID!
  }
  input MaestroInput {
    nombre: String!
    paterno: String!
    materno: String!
    email: String!
    telefono: String
    celular: String!
    curp: String!
    sangre: String!
    alergias: String!
    padecimientos: String!
  }

  input MasEstudianteInput {
    nombre: String!
    paterno: String!
    materno: String!
    email: String!
    nacimiento: String!
    semestre: String!
    genero: String!
    curp: String!
    sangre: String!
    alergias: String!
    padecimientos: String!
  }

  input FemEstudianteInput {
    nombre: String!
    paterno: String!
    materno: String!
    email: String!
    nacimiento: String!
    semestre: String!
    genero: String!
    curp: String!
    sangre: String!
    alergias: String!
    padecimientos: String!
  }

  input PedidoProductoInput {
    id: ID
    cantidad: Int
  }

  input PedidoInput {
    pedido: [PedidoProductoInput]
    total: Float
    estudiante: ID
    estado: EstadoPedido
  }
  input HistorialInput {
    pedido: [PedidoProductoInput]
    total: Int
    estudiante: ID
    estado: EstadoPedido
  }

  enum EstadoPedido {
    PENDIENTE
    COMPLETADO
    CANCELADO
  }

  type Query {
    #Usuarios
    obtenerUsuario(token: String!): Usuario
    obtenerUsuarioEmail(email: String!): Usuario

    #Clubes
    obtenerClubes: [Club]
    obtenerClub(id: ID!): Club
    obtenerClubesMasculinos: [ClubMasculino]
    obtenerClubMasculino(id: ID!): ClubMasculino
    obtenerClubesFemeninos: [ClubFemenino]
    obtenerClubFemenino(id: ID!): ClubFemenino

    #Talleres
    obtenerTalleres: [Taller]
    obtenerTaller(id: ID!): Taller
    obtenerTalleresMaestro(id: ID!): [Taller]

    #Estudiantes
    obtenerEstudiantes: [Estudiante]
    obtenerEstudiantesUsuario: [Estudiante]
    obtenerEstudiante(id: ID!): Estudiante
    obtenerEstudianteEmail(email: String!): Estudiante

    #Maestros
    obtenerMaestros: [Maestro]
    obtenerMaestrosUsuario: [Maestro]
    obtenerMaestro(id: ID!): Maestro

    #Estudiantes Mas
    obtenerMasEstudiantes: [MasEstudiante]
    obtenerMasEstudiantesUsuario: [MasEstudiante]
    obtenerMasEstudiante(id: ID!): MasEstudiante

    #Estudiantes Fem
    obtenerFemEstudiantes: [FemEstudiante]
    obtenerFemEstudiantesUsuario: [FemEstudiante]
    obtenerFemEstudiante(id: ID!): FemEstudiante

    #Pedidos
    obtenerPedidos: [Pedido]
    obtenerPedidosEstudiante: [Pedido]
    obtenerPedido(id: ID!): Pedido
    obtenerPedidosEstado(estado: String!): [Pedido]
    obtenerPedidosTaller(id: ID!): [Pedido]
    obtenerPedidosEmail(email: String!): [Pedido]

    #Busquedas Avanzadas
    mejoresEstudiantes: [TopEstudiante]
    mejoresGrupos: [TopGrupo]
    buscarTaller(texto: String!): [Taller]
    buscarClub(texto: String!): [Club]
    buscarClubFemenino(texto: String!): [ClubFemenino]
    buscarClubMasculino(texto: String!): [ClubMasculino]
    buscarEstudiante(texto: String!): [Estudiante]
    buscarMaestro(texto: String!): [Maestro]

    #Historial
    obtenerHistoriales: [Historial]
    obtenerHistorial(id: ID!): Historial
    obtenerHistorialUsuario: [Historial]
  }

  type Mutation {
    # Usuarios
    nuevoUsuario(input: UsuarioInput): Usuario
    autenticarUsuario(input: AutenticarInput): Token

    #Curso
    nuevoCurso(input: CursoInput): Curso

    #Clubes
    nuevoClub(input: ClubInput): Club
    actualizarClub(id: ID!, input: ClubInput): Club
    eliminarClub(id: ID!): String
    nuevoClubMasculino(input: ClubMasculinoInput): Club
    actualizarClubMasculino(id: ID!, input: ClubMasculinoInput): Club
    eliminarClubMasculino(id: ID!): String
    nuevoClubFemenino(input: ClubFemeninoInput): Club
    actualizarClubFemenino(id: ID!, input: ClubFemeninoInput): Club
    eliminarClubFemenino(id: ID!): String

    #Talleres
    nuevoTaller(input: TallerInput): Taller
    actualizarTaller(id: ID!, input: TallerInput): Taller
    eliminarTaller(id: ID!): String

    #Estudiantes
    nuevoEstudiante(input: EstudianteInput): Estudiante
    actualizarEstudiante(id: ID!, input: EstudianteInput): Estudiante
    actualizarGpoEst(id: ID!, input: ActualizarGpoEstInput): Estudiante
    eliminarEstudiante(id: ID!): String

    #Maestros
    nuevoMaestro(input: MaestroInput): Maestro
    actualizarMaestro(id: ID!, input: MaestroInput): Maestro
    eliminarMaestro(id: ID!): String

    #Estudiantes Mas
    nuevoMasEstudiante(input: MasEstudianteInput): MasEstudiante
    actualizarMasEstudiante(id: ID!, input: MasEstudianteInput): MasEstudiante
    eliminarMasEstudiante(id: ID!): String

    #Estudiantes Fem
    nuevoFemEstudiante(input: FemEstudianteInput): FemEstudiante
    actualizarFemEstudiante(id: ID!, input: FemEstudianteInput): FemEstudiante
    eliminarFemEstudiante(id: ID!): String

    #Pedidos
    nuevoPedido(input: PedidoInput): Pedido
    actualizarPedido(id: ID!, input: PedidoInput): Pedido
    eliminarPedido(id: ID!): String

    #Historial
    nuevoHistorial(input: HistorialInput): Historial
    actualizarHistorial(id: ID!, input: HistorialInput): Historial
  }
`;

module.exports = typeDefs;
