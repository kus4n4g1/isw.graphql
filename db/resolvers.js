const Usuario = require('../models/Usuario');
const Curso = require('../models/Curso');
const Club = require('../models/Club');
const ClubMasculino = require('../models/ClubMasculino');
const ClubFemenino = require('../models/ClubFemenino');
const Estudiante = require('../models/Estudiante');
const Maestro = require('../models/Maestro');
const FemEstudiante = require('../models/FemEstudiante');
const MasEstudiante = require('../models/MasEstudiante');
const Historial = require('../models/Historial');

const Pedido = require('../models/Pedido');
const Taller = require('../models/Taller');

const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
require('dotenv').config({ path: 'variables.env' });

const crearToken = (usuario, secreta, expiresIn) => {
  console.log(usuario);
  const { id, email, nombre, apellido } = usuario;

  return jwt.sign({ id, email, nombre, apellido }, secreta, { expiresIn });
};
//resolver
const resolvers = {
  Query: {
    obtenerUsuario: async (_, { token }) => {
      const usuarioId = await jwt.verify(token, process.env.SECRETA);

      return usuarioId;
    },
    obtenerUsuarioEmail: async (_, { email }) => {
      try {
        const usuarioEmail = await Usuario.findOne({ email });
        return usuarioEmail;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerClubes: async (_, { input }) => {
      try {
        const clubes = await Club.find({}).populate('maestro');
        return clubes;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerClub: async (_, { id }) => {
      //revisar si el producto existe o no
      const club = await Club.findById(id).populate('maestro');

      if (!club) {
        throw new Error('Club no encontrado');
      }

      return club;
    },
    obtenerClubesMasculinos: async (_, { input }) => {
      try {
        const clubesMasculinos = await ClubMasculino.find({}).populate(
          'maestro'
        );
        return clubesMasculinos;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerClubMasculino: async (_, { id }) => {
      //revisar si el producto existe o no
      const clubMasculino = await ClubMasculino.findById(id).populate(
        'maestro'
      );

      if (!clubMasculino) {
        throw new Error('Club no encontrado');
      }

      return clubMasculino;
    },
    obtenerClubesFemeninos: async (_, { input }) => {
      try {
        const clubesFemeninos = await ClubFemenino.find({}).populate('maestro');
        return clubesFemeninos;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerClubFemenino: async (_, { id }) => {
      //revisar si el producto existe o no
      const clubFemenino = await ClubFemenino.findById(id).populate('maestro');

      if (!clubFemenino) {
        throw new Error('Club no encontrado');
      }

      return clubFemenino;
    },

    obtenerTalleres: async (_, { input }) => {
      try {
        const talleres = await Taller.find({}).populate('maestro');
        return talleres;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerTaller: async (_, { id }) => {
      //revisar si el taller existe o no
      const taller = await Taller.findById(id).populate('maestro');
      if (!taller) {
        throw new Error('Taller no encontrado');
      }
      return taller;
    },
    obtenerTalleresMaestro: async (_, { id }) => {
      console.log('Este es el id de maestro', id);
      //Verificar si maestro existe o no
      let maestroExiste = await Maestro.findById(id);

      if (!maestroExiste) {
        throw new Error('Ese Maestro no existe');
      }
      //Verificar si el maestro es parte del grupo
      // if (maestroExiste.grupo.toString() !== ctx.usuario.id) {
      //   throw new Error('No tienes las credenciales');
      // }
      // const tallerExiste = await Taller.find({
      //   maestro: id,
      // }).populate('maestro');
      //revisar si el taller existe o no
      let tallerExiste = Taller.aggregate([
        {
          $lookup: {
            from: 'maestro',
            localField: 'maestro',
            foreignField: '_id',
            as: 'in_common',
          },
        },
      ]);

      console.log('Esto es Taller con maestros existe' + tallerExiste);

      // let tallerExiste = await Taller.find({
      //   'maestro.id': id,
      // }).populate('maestro');

      if (!tallerExiste) {
        throw new Error('Taller no encontrado');
      }

      //Retornar taller o talleres encontrados
      return tallerExiste;
    },
    obtenerEstudiantes: async () => {
      try {
        const estudiantes = await Estudiante.find({});
        return estudiantes;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerEstudiantesUsuario: async (_, {}, ctx) => {
      try {
        const estudiantes = await Estudiante.find({
          grupo: ctx.usuario.id.toString(),
        });
        return estudiantes;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerEstudianteEmail: async (_, { email }) => {
      try {
        const estudianteEmail = await Estudiante.findOne({ email });
        console.log(estudianteEmail);
        return estudianteEmail;
      } catch (error) {
        console.log(error);
      }
    },
    // obtenerEstudiante: async (_, { id }, ctx) => {

    obtenerEstudiante: async (_, { id }) => {
      //Revisar si el Estudiante existe o no
      const estudiante = await Estudiante.findById(id);

      if (!estudiante) {
        throw new Error('Estudiante no encontrado');
      }

      //Quien lo creo puede verlo
      // if (estudiante.grupo.toString() !== ctx.usuario.id) {
      //   throw new Error('No tienes las credenciales');
      // }

      return estudiante;
    },
    obtenerMaestros: async () => {
      try {
        const maestros = await Maestro.find({});
        return maestros;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerMaestrosUsuario: async (_, {}, ctx) => {
      try {
        const maestros = await Maestro.find({
          grupo: ctx.usuario.id.toString(),
        });
        return maestros;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerMaestro: async (_, { id }) => {
      //Revisar si el Estudiante existe o no
      const maestro = await Maestro.findById(id);

      if (!maestro) {
        throw new Error('Maestro no encontrado');
      }

      //Quien lo creo puede verlo
      // if (maestro.grupo.toString() !== ctx.usuario.id) {
      //   throw new Error('No tienes las credenciales');
      // }

      return maestro;
    },
    obtenerMasEstudiantes: async () => {
      try {
        const masEstudiantes = await MasEstudiante.find({});
        return masEstudiantes;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerMasEstudiantesUsuario: async (_, {}, ctx) => {
      try {
        const masEstudiantes = await MasEstudiante.find({
          grupo: ctx.usuario.id.toString(),
        });
        return masEstudiantes;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerMasEstudiante: async (_, { id }, ctx) => {
      //Revisar si el Estudiante existe o no
      const masEstudiante = await MasEstudiante.findById(id);

      if (!masEstudiante) {
        throw new Error('Estudiante no encontrado');
      }

      //Quien lo creo puede verlo
      if (masEstudiante.grupo.toString() !== ctx.usuario.id) {
        throw new Error('No tienes las credenciales');
      }

      return masEstudiante;
    },
    obtenerFemEstudiantes: async () => {
      try {
        const femEstudiantes = await FemEstudiante.find({});
        return femEstudiantes;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerFemEstudiantesUsuario: async (_, {}, ctx) => {
      try {
        const femEstudiantes = await FemEstudiante.find({
          grupo: ctx.usuario.id.toString(),
        });
        return femEstudiantes;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerFemEstudiante: async (_, { id }, ctx) => {
      //Revisar si el Estudiante existe o no
      const femEstudiante = await FemEstudiante.findById(id);

      if (!femEstudiante) {
        throw new Error('Estudiante no encontrado');
      }

      //Quien lo creo puede verlo
      if (femEstudiante.grupo.toString() !== ctx.usuario.id) {
        throw new Error('No tienes las credenciales');
      }

      return femEstudiante;
    },
    obtenerPedidos: async () => {
      try {
        const pedidos = await Pedido.find({}).populate('estudiante');
        return pedidos;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerPedidosEmail: async (_, { email }) => {
      try {
        const pedidos = await Pedido.find({
          'estudiante.email': email,
        }).populate('estudiante');
        return pedidos;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerPedidosEstudiante: async (_, {}, ctx) => {
      try {
        const pedidos = await Pedido.find({ grupo: ctx.usuario.id });
        return pedidos;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerPedidosTaller: async (_, { id }) => {
      try {
        //const pedidos = await Pedido.find({ pedido: [{ id: { id } }] });
        //const pedidos = await Pedido.find((pedido) => pedido.pedido.id === +id);
        //const pedidos = await Pedido.find({ pedido: { $in: [id === id] } });
        const pedidos = await Pedido.find({ 'pedido.id': id }).populate(
          'estudiante'
        );
        console.log(pedidos);
        //const pedidos = await Pedido.find(pedido.id === id);
        return pedidos;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerPedido: async (_, { id }, ctx) => {
      //verificar si el pedido existe o no
      const pedido = await Pedido.findById(id);

      if (!pedido) {
        throw new Error('Pedido no encontrado');
      }

      //Solo quien lo creo puede verlo
      if (pedido.grupo.toString() !== ctx.usuario.id) {
        throw new Error('No tienes las credenciales');
      }

      //retornar el resultado
      return pedido;
    },
    obtenerHistorial: async (_, { id }, ctx) => {
      try {
        const historial = await Historial.findOne({ estudiante: id });
        return historial;
      } catch (error) {
        console.log(error);
      }
    },

    obtenerHistorialUsuario: async (_, {}, ctx) => {
      try {
        console.log('Este es el context', ctx.usuario.id);
        const historiales = await Historial.find({
          grupo: ctx.usuario.id,
        });
        console.log(historiales);
        return historiales;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerHistoriales: async () => {
      try {
        const historiales = await Historial.find({});
        return historiales;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerPedidosEstado: async (_, { estado }, ctx) => {
      const pedidos = await Pedido.find({
        estudiante: ctx.usuario.id,
        estado,
      });

      return pedidos;
    },
    mejoresEstudiantes: async () => {
      const estudiantes = await Pedido.aggregate([
        { $match: { estado: 'COMPLETADO' } },
        {
          $group: {
            _id: '$estudiante',
            total: { $sum: '$total' },
          },
        },
        {
          $lookup: {
            from: 'estudiantes',
            localField: '_id',
            foreignField: '_id',
            as: 'estudiante',
          },
        },
        {
          $limit: 10,
        },
        {
          $sort: { total: -1 },
        },
      ]);

      return estudiantes;
    },
    mejoresGrupos: async () => {
      const grupos = await Pedido.aggregate([
        { $match: { estado: 'COMPLETADO' } },
        {
          $group: {
            _id: '$grupo',
            total: { $sum: '$total' },
          },
        },
        {
          $lookup: {
            from: 'usuarios',
            localField: '_id',
            foreignField: '_id',
            as: 'grupo',
          },
        },
        {
          $limit: 3,
        },
        {
          $sort: { total: -1 },
        },
      ]);

      return grupos;
    },
    buscarTaller: async (_, { texto }) => {
      const talleres = await Taller.find({ $text: { $search: texto } }).limit(
        10
      );

      return talleres;
    },
    buscarClub: async (_, { texto }) => {
      const clubes = await Club.find({ $text: { $search: texto } }).limit(10);

      return clubes;
    },
    buscarClubFemenino: async (_, { texto }) => {
      const clubes = await ClubFemenino.find({
        $text: { $search: texto },
      }).limit(10);

      return clubes;
    },
    buscarClubMasculino: async (_, { texto }) => {
      const clubes = await ClubMasculino.find({
        $text: { $search: texto },
      }).limit(10);

      return clubes;
    },
    buscarEstudiante: async (_, { texto }) => {
      const estudiantes = await Estudiante.find({
        $text: { $search: texto },
      }).limit(10);

      return estudiantes;
    },
    buscarMaestro: async (_, { texto }) => {
      const maestros = await Maestro.find({ $text: { $search: texto } }).limit(
        10
      );

      return maestros;
    },
  },

  Mutation: {
    nuevoUsuario: async (_, { input }) => {
      const { email, password } = input;

      //Revisar si el usuario ya está registrado
      const existeUsuario = await Usuario.findOne({ email });
      if (existeUsuario) {
        throw new Error('El usuario ya está registrado');
      }

      //Hashear su password
      const salt = await bcryptjs.genSalt(10);
      input.password = await bcryptjs.hash(password, salt);

      try {
        //Guardarlo en la base de datos
        const usuario = new Usuario(input);
        usuario.save(); // guardarlo
        return usuario;
      } catch (error) {
        console.log(error);
      }
    },
    autenticarUsuario: async (_, { input }) => {
      const { email, password } = input;
      // Si el usuario existe
      const existeUsuario = await Usuario.findOne({ email });
      if (!existeUsuario) {
        throw new Error('El usuario no existe');
      }

      // Revisara si el password es correcto
      const passwordCorrecto = await bcryptjs.compare(
        password,
        existeUsuario.password
      );
      if (!passwordCorrecto) {
        throw new Error('El Password es Incorrecto');
      }
      // const existePedido = await Pedido.findOne({})
      //   .populate({
      //     path: 'estudiante',
      //     match: {
      //       email: email,
      //     },
      //   })
      //   .exec((err, estudiante) => {
      //     console.log('Estudiante populado ' + estudiante);
      //     console.log('Este es existe pedido ' + existePedido);
      //   });
      // console.log(existePedido);
      // if (existePedido) {
      //   throw new Error('El estudiante ya tiene club');
      // }

      // Crear el token
      return {
        token: crearToken(existeUsuario, process.env.SECRETA, '24h'),
      };
    },
    nuevoCurso: async (_, { input }) => {
      try {
        const curso = new Curso(input);

        //almacenar en la bd
        const resultado = await curso.save();

        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    nuevoClub: async (_, { input }) => {
      const { maestro } = input;
      //Verificar si maestro existe o no
      let maestroExiste = await Maestro.findById(maestro);

      if (!maestroExiste) {
        throw new Error('Ese maestro no existe');
      }
      // //Verificar si el maestro es parte del grupo
      // if (maestroExiste.grupo.toString() !== ctx.usuario.id) {
      //   throw new Error('No tienes las credenciales');
      // }
      try {
        // Crear un nuevo club
        const nuevoClub = new Club(input);

        //Asignarle un grupo
        // nuevoTaller.grupo = ctx.usuario.id;

        //Guardarlo en la base de datos
        const resultado = await nuevoClub.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarClub: async (_, { id, input }) => {
      //revisar si el club existe o no
      let club = await Club.findById(id);

      if (!club) {
        throw new Error('Club no encontrado');
      }

      //guardarlo en la base de datos
      club = await Club.findOneAndUpdate({ _id: id }, input, { new: true });

      return club;
    },

    eliminarClub: async (_, { id }) => {
      //revisar si el club existe o no
      let club = await Club.findById(id);

      if (!club) {
        throw new Error('Club no encontrado');
      }

      await Club.findOneAndDelete({ _id: id });

      return 'Club eliminado';
    },
    nuevoClubMasculino: async (_, { input }) => {
      const { maestro } = input;
      //Verificar si maestro existe o no
      let maestroExiste = await Maestro.findById(maestro);

      if (!maestroExiste) {
        throw new Error('Ese maestro no existe');
      }
      // //Verificar si el maestro es parte del grupo
      // if (maestroExiste.grupo.toString() !== ctx.usuario.id) {
      //   throw new Error('No tienes las credenciales');
      // }
      try {
        // Crear un nuevo club masculino
        const nuevoClubMasculino = new ClubMasculino(input);

        //Asignarle un grupo
        // nuevoTaller.grupo = ctx.usuario.id;

        //Guardarlo en la base de datos
        const resultado = await nuevoClubMasculino.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarClubMasculino: async (_, { id, input }) => {
      //revisar si el club existe o no
      let clubMasculino = await ClubMasculino.findById(id);

      if (!clubMasculino) {
        throw new Error('Club no encontrado');
      }

      //guardarlo en la base de datos
      clubMasculino = await ClubMasculino.findOneAndUpdate({ _id: id }, input, {
        new: true,
      });

      return clubMasculino;
    },

    eliminarClubMasculino: async (_, { id }) => {
      //revisar si el club existe o no
      let clubMasculino = await ClubMasculino.findById(id);

      if (!clubMasculino) {
        throw new Error('Club no encontrado');
      }

      await ClubMasculino.findOneAndDelete({ _id: id });

      return 'Club eliminado';
    },
    nuevoClubFemenino: async (_, { input }) => {
      const { maestro } = input;
      //Verificar si maestro existe o no
      let maestroExiste = await Maestro.findById(maestro);

      if (!maestroExiste) {
        throw new Error('Ese maestro no existe');
      }
      // //Verificar si el maestro es parte del grupo
      // if (maestroExiste.grupo.toString() !== ctx.usuario.id) {
      //   throw new Error('No tienes las credenciales');
      // }
      try {
        // Crear un nuevo club femenino
        const nuevoClubFemenino = new ClubFemenino(input);

        //Asignarle un grupo
        // nuevoTaller.grupo = ctx.usuario.id;

        //Guardarlo en la base de datos
        const resultado = await nuevoClubFemenino.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarClubFemenino: async (_, { id, input }) => {
      //revisar si el club existe o no
      let clubFemenino = await ClubFemenino.findById(id);

      if (!clubFemenino) {
        throw new Error('Club no encontrado');
      }

      //guardarlo en la base de datos
      clubFemenino = await ClubFemenino.findOneAndUpdate({ _id: id }, input, {
        new: true,
      });

      return clubFemenino;
    },

    eliminarClubFemenino: async (_, { id }) => {
      //revisar si el club existe o no
      let clubFemenino = await ClubFemenino.findById(id);

      if (!clubFemenino) {
        throw new Error('Club no encontrado');
      }

      await ClubFemenino.findOneAndDelete({ _id: id });

      return 'Club eliminado';
    },

    nuevoTaller: async (_, { input }) => {
      const { maestro } = input;
      //Verificar si maestro existe o no
      let maestroExiste = await Maestro.findById(maestro);

      if (!maestroExiste) {
        throw new Error('Ese maestro no existe');
      }
      // //Verificar si el maestro es parte del grupo
      // if (maestroExiste.grupo.toString() !== ctx.usuario.id) {
      //   throw new Error('No tienes las credenciales');
      // }
      try {
        // Crear un nuevo taller
        const nuevoTaller = new Taller(input);

        //Asignarle un grupo
        // nuevoTaller.grupo = ctx.usuario.id;

        //Guardarlo en la base de datos
        const resultado = await nuevoTaller.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },

    actualizarTaller: async (_, { id, input }) => {
      //revisar si el taller existe o no
      let taller = await Taller.findById(id);

      //guardar en base de datos
      if (!taller) {
        throw new Error('Taller no encontrado');
      }

      //Guardarlo en  bd
      taller = await Taller.findOneAndUpdate({ _id: id }, input, { new: true });

      return taller;
    },

    eliminarTaller: async (_, { id }) => {
      //revisar si el taller existe o no
      let taller = await Taller.findById(id);

      if (!taller) {
        throw new Error('Taller no encontrado');
      }

      await Taller.findOneAndDelete({ _id: id });

      return 'Taller Eliminado';
    },
    nuevoEstudiante: async (_, { input }, ctx) => {
      console.log(ctx);

      const { email } = input;

      //Verificar si el estudiante ya está registrado
      //console.log(input);

      const estudiante = await Estudiante.findOne({ email });

      if (estudiante) {
        throw new Error('Ese estudiante ya está registrado');
      }

      const nuevoEstudiante = new Estudiante(input);

      //Asignar el usuario
      nuevoEstudiante.grupo = ctx.usuario.id;

      //Guardarlo en BD

      try {
        const resultado = await nuevoEstudiante.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarEstudiante: async (_, { id, input }, ctx) => {
      //Verificar si existe o no
      let estudiante = await Estudiante.findById(id);

      if (!estudiante) {
        throw new Error('Ese estudiante no existe');
      }
      //Verificar si el Usuario es quien edita
      // if (estudiante.grupo.toString() !== ctx.usuario.id) {
      //   throw new Error('No tienes las credenciales');
      // }

      //Guardar el Estudiante en BD
      estudiante = await Estudiante.findOneAndUpdate({ _id: id }, input, {
        new: true,
      });
      return estudiante;
    },
    actualizarGpoEst: async (_, { id, input }) => {
      //Verificar si existe o no
      let estudiante = await Estudiante.findById(id);

      if (!estudiante) {
        throw new Error('Ese estudiante no existe');
      }

      //Guardar el Estudiante en BD
      estudiante = await Estudiante.findOneAndUpdate({ _id: id }, input, {
        new: true,
      });
      return estudiante;
    },
    eliminarEstudiante: async (_, { id }, ctx) => {
      //Verificar si existe o no
      let estudiante = await Estudiante.findById(id);
      console.log('Esto es estudiante' + estudiante);
      if (!estudiante) {
        throw new Error('Ese estudiante no existe');
      }
      // //Verificar si el Usuario es quien edita
      // if (estudiante.grupo.toString() !== ctx.usuario.id) {
      //   throw new Error('No tienes las credenciales');
      // }

      //eliminar Estudiante
      await Estudiante.findOneAndDelete({ _id: id });
      return 'Estudiante Eliminado';
    },
    nuevoMaestro: async (_, { input }, ctx) => {
      console.log(ctx);

      const { email } = input;

      //Verificar si el maestro ya está registrado
      //console.log(input);

      const maestro = await Maestro.findOne({ email });

      if (maestro) {
        throw new Error('Ese maestro ya está registrado');
      }

      const nuevoMaestro = new Maestro(input);

      //Asignar el usuario
      nuevoMaestro.grupo = ctx.usuario.id;

      //Guardarlo en BD

      try {
        const resultado = await nuevoMaestro.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarMaestro: async (_, { id, input }, ctx) => {
      //Verificar si existe o no
      let maestro = await Maestro.findById(id);

      if (!maestro) {
        throw new Error('Ese estudiante no existe');
      }
      //Verificar si el Usuario es quien edita
      if (maestro.grupo.toString() !== ctx.usuario.id) {
        throw new Error('No tienes las credenciales');
      }

      //Guardar el maestro en BD
      estudiante = await Maestro.findOneAndUpdate({ _id: id }, input, {
        new: true,
      });
      return maestro;
    },
    eliminarMaestro: async (_, { id }, ctx) => {
      //Verificar si existe o no
      let maestro = await Maestro.findById(id);

      if (!maestro) {
        throw new Error('Ese estudiante no existe');
      }
      Z;
      //eliminar Estudiante
      await Maestro.findOneAndDelete({ _id: id });
      return 'Maestro Eliminado';
    },
    nuevoMasEstudiante: async (_, { input }, ctx) => {
      console.log(ctx);

      const { email } = input;

      //Verificar si el estudiante ya está registrado
      //console.log(input);

      const masEstudiante = await MasEstudiante.findOne({ email });

      if (masEstudiante) {
        throw new Error('Ese estudiante ya está registrado');
      }

      const nuevoMasEstudiante = new MasEstudiante(input);

      //Asignar el usuario
      nuevoMasEstudiante.grupo = ctx.usuario.id;

      //Guardarlo en BD

      try {
        const resultado = await nuevoMasEstudiante.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarMasEstudiante: async (_, { id, input }, ctx) => {
      //Verificar si existe o no
      let masEstudiante = await MasEstudiante.findById(id);

      if (!masEstudiante) {
        throw new Error('Ese estudiante no existe');
      }
      //Verificar si el Usuario es quien edita
      if (masEstudiante.grupo.toString() !== ctx.usuario.id) {
        throw new Error('No tienes las credenciales');
      }

      //Guardar el Estudiante en BD
      masEstudiante = await MasEstudiante.findOneAndUpdate({ _id: id }, input, {
        new: true,
      });
      return masEstudiante;
    },
    // eliminarEstudiante: async (_, { id }, ctx) => {
    //   //Verificar si existe o no
    //   let masEstudiante = await MasEstudiante.findById(id);
    //   if (!masEstudiante) {
    //     throw new Error('Ese estudiante no existe');
    //   }
    //   //Verificar si el Usuario es quien edita
    //   if (masEstudiante.grupo.toString() !== ctx.usuario.id) {
    //     throw new Error('No tienes las credenciales');
    //   }

    //   //eliminar Estudiante
    //   await masEstudiante.findOneAndDelete({ _id: id });
    //   return 'Estudiante Eliminado';
    // },
    nuevoFemEstudiante: async (_, { input }, ctx) => {
      console.log(ctx);

      const { email } = input;

      //Verificar si el estudiante ya está registrado
      //console.log(input);

      const femEstudiante = await FemEstudiante.findOne({ email });

      if (femEstudiante) {
        throw new Error('Ese estudiante ya está registrado');
      }

      const nuevoFemEstudiante = new FemEstudiante(input);

      //Asignar el usuario
      nuevoFemEstudiante.grupo = ctx.usuario.id;

      //Guardarlo en BD

      try {
        const resultado = await nuevoFemEstudiante.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarFemEstudiante: async (_, { id, input }, ctx) => {
      //Verificar si existe o no
      let femEstudiante = await FemEstudiante.findById(id);

      if (!femEstudiante) {
        throw new Error('Ese estudiante no existe');
      }
      //Verificar si el Usuario es quien edita
      if (femEstudiante.grupo.toString() !== ctx.usuario.id) {
        throw new Error('No tienes las credenciales');
      }

      //Guardar el Estudiante en BD
      femEstudiante = await FemEstudiante.findOneAndUpdate({ _id: id }, input, {
        new: true,
      });
      return femEstudiante;
    },
    eliminarFemEstudiante: async (_, { id }, ctx) => {
      //Verificar si existe o no
      let femEstudiante = await FemEstudiante.findById(id);

      if (!femEstudiante) {
        throw new Error('Ese estudiante no existe');
      }
      //Verificar si el Usuario es quien edita
      if (femEstudiante.grupo.toString() !== ctx.usuario.id) {
        throw new Error('No tienes las credenciales');
      }

      //eliminar Estudiante
      await FemEstudiante.findOneAndDelete({ _id: id });
      return 'Estudiante Eliminado';
    },
    nuevoPedido: async (_, { input }, ctx) => {
      const { estudiante } = input;

      //Verificar si estudiante existe o no
      let estudianteExiste = await Estudiante.findById(estudiante);

      if (!estudianteExiste) {
        throw new Error('Ese estudiante no existe');
      }
      //Verificar si el estudiante es parte del grupo
      if (estudianteExiste.grupo.toString() !== ctx.usuario.id) {
        throw new Error('No tienes las credenciales');
      }

      //Revisar que haya cupo disponible

      for await (const articulo of input.pedido) {
        const { id } = articulo;

        const taller = await Taller.findById(id);
        const club = await Club.findById(id);
        const clubFemenino = await ClubFemenino.findById(id);
        const clubMasculino = await ClubMasculino.findById(id);
        if (taller) {
          if (articulo.cantidad > taller.existencia) {
            throw new Error(`El Taller: ${taller.nombre} no tiene cupo`);
          } else {
            // Restar la cantidad a lo disponible
            taller.existencia = taller.existencia - articulo.cantidad;

            await taller.save();
          }
        }
        if (club) {
          if (articulo.cantidad > club.existencia) {
            throw new Error(`El Club: ${club.nombre} no tiene cupo`);
          } else {
            // Restar la cantidad a lo disponible
            club.existencia = club.existencia - articulo.cantidad;

            await club.save();
          }
        }
        if (clubFemenino) {
          if (articulo.cantidad > clubFemenino.existencia) {
            throw new Error(`El Club: ${clubFemenino.nombre} no tiene cupo`);
          } else {
            // Restar la cantidad a lo disponible
            clubFemenino.existencia =
              clubFemenino.existencia - articulo.cantidad;

            await clubFemenino.save();
          }
        }
        if (clubMasculino) {
          if (articulo.cantidad > clubMasculino.existencia) {
            throw new Error(`El Club: ${clubMasculino.nombre} no tiene cupo`);
          } else {
            // Restar la cantidad a lo disponible
            clubMasculino.existencia =
              clubMasculino.existencia - articulo.cantidad;

            await clubMasculino.save();
          }
        }
      }
      // Crear un nuevo pedido
      const nuevoPedido = new Pedido(input);

      //Asignarle un grupo
      nuevoPedido.grupo = ctx.usuario.id;

      //Guardarlo en la base de datos
      const resultado = await nuevoPedido.save();
      return resultado;
    },
    actualizarPedido: async (_, { id, input }, ctx) => {
      const { estudiante } = input;

      // //verificar si el pedido existe
      // const existePedido = await Pedido.findOne(estudiante);

      // if (!existePedido) {
      //   throw new Error('El pedido no existe');
      // }

      //Si el estudiante existe
      const existeEstudiante = await Estudiante.findById(estudiante);

      if (!existeEstudiante) {
        throw new Error('El estudiante no existe');
      }

      // //Si el estudiante y pedido pertenece al estudiante
      // if (existeEstudiante.grupo.toString() !== ctx.usuario.id) {
      //   throw new Error('No tienes las credenciales');
      // }
      //Revisar el cupo
      if (input.pedido) {
        for await (const articulo of input.pedido) {
          const { id } = articulo;

          const taller = await Taller.findById(id);
          const club = await club.findById(id);
          const clubFemenino = await clubFemenino.findById(id);
          const clubMasculino = await clubMasculino.findById(id);
          console.log('Esto es club', club);
          if (taller) {
            if (articulo.cantidad > taller.existencia) {
              throw new Error(
                `El Taller: ${taller.nombre} excede el cupo disponible`
              );
            } else {
              // Restar la cantidad a lo disponible
              taller.existencia = taller.existencia - articulo.cantidad;

              await taller.save();
            }
          }
          if (club) {
            if (articulo.cantidad > club.existencia) {
              throw new Error(
                `El Club: ${club.nombre} excede el cupo disponible`
              );
            } else {
              // Restar la cantidad a lo disponible
              club.existencia = club.existencia - articulo.cantidad;

              await club.save();
            }
          }
          if (clubFemenino) {
            if (articulo.cantidad > clubFemenino.existencia) {
              throw new Error(
                `El Club: ${clubFemenino.nombre} excede el cupo disponible`
              );
            } else {
              // Restar la cantidad a lo disponible
              clubFemenino.existencia =
                clubFemenino.existencia - articulo.cantidad;

              await clubFemenino.save();
            }
          }
          if (clubMasculino) {
            if (articulo.cantidad > clubMasculino.existencia) {
              throw new Error(
                `El Club: ${clubMasculino.nombre} excede el cupo disponible`
              );
            } else {
              // Restar la cantidad a lo disponible
              clubMasculino.existencia =
                clubMasculino.existencia - articulo.cantidad;

              await clubMasculino.save();
            }
          }
        }
      }
      //Guardar el pedido
      const resultado = await Pedido.findOneAndUpdate({ _id: id }, input, {
        new: true,
      });
      return resultado;
    },
    eliminarPedido: async (_, { id }, ctx) => {
      //Verificar si existe o no
      const pedido = await Pedido.findById(id);

      if (!pedido) {
        throw new Error('Ese pedido no existe');
      }
      //Verificar si el Usuario es quien edita
      // if (pedido.grupo.toString() !== ctx.usuario.id) {
      //   throw new Error('No tienes las credenciales');
      // }

      //eliminar Pedido
      await Pedido.findOneAndDelete({ _id: id });
      return 'Pedido Eliminado';
    },
    nuevoHistorial: async (_, { input }, ctx) => {
      const { estudiante } = input;

      //Verificar si estudiante existe o no
      let estudianteExiste = await Estudiante.findById(estudiante);

      if (!estudianteExiste) {
        throw new Error('Ese estudiante no existe');
      }
      //Verificar si el estudiante es parte del grupo
      if (estudianteExiste.grupo.toString() !== ctx.usuario.id) {
        throw new Error('No tienes las credenciales');
      }
      //Revisar que haya cupo disponible
      // let historialExiste = await Historial.find({
      //   estudiante: estudiante.id,
      // })
      //   .populate({
      //     path: 'estudiante',
      //   })
      //   .exec(function (err, couple) {
      //     if (err) res.send(err);
      //     res.json(couple);
      //   });

      // let historialExiste = await Historial.find({ estudiante: estudiante })
      //   .populate('estudiante')
      //   .exec(function (err, docs) {
      //     //console.log(docs[0].estudiante.nombre);
      //     console.log('Esto es docs' + docs);
      //   });
      // let miPrimeraPromise = new Promise((resolve, reject) => {

      // });
      // function otherFunction(data) {
      //   historialExiste = data;
      // }

      // let historialExiste = await Historial.findOne({}).populate({
      //   path: 'estudiante',
      //   match: estudiante,
      // });
      let historialExiste = await Historial.find({ grupo: ctx.usuario.id });
      // const historiales = await Historial.find({
      //   grupo: ctx.usuario.id,
      // });
      // .exec(function (err, data) {
      //   if (err) {
      //     console.log(err);
      //   }
      //   otherFunction(data);
      //   console.log('Esto es data ' + data);
      // });

      console.log('Esto es historial existe' + historialExiste);
      console.log('Esto es historial existe length', historialExiste.length);
      if (historialExiste == 0) {
        console.log('No existe historialExiste');
        // Crear un nuevo historial
        const nuevoHistorial = new Historial(input);

        //Asignarle un grupo
        nuevoHistorial.grupo = ctx.usuario.id;
        //taller.existencia = taller.existencia - articulo.cantidad;

        //await taller.save();
        //Guardarlo en la base de datos
        const resultado = await nuevoHistorial.save();
        return resultado;
      } else {
        if (historialExiste.length == 2) {
          throw new Error('Talleres y Clubs concluidos');
        }

        if (historialExiste.length == 1) {
          console.log(
            'Esta entrando la condicion de historial agregado o no???'
          );
          const nuevoHistorial = new Historial(input);

          //Asignarle un grupo
          nuevoHistorial.grupo = ctx.usuario.id;
          //taller.existencia = taller.existencia - articulo.cantidad;

          //await taller.save();
          //Guardarlo en la base de datos
          const resultado = await nuevoHistorial.save();
          return resultado;
          // for await (const articulo of input.pedido) {
          //   const { id, cantidad } = articulo;
          //   console.log('verificación de total == 1:  ' + id);
          //   const taller = await Taller.findById(id);
          //   const club = await Club.findById(id);
          //   const clubFemenino = await ClubFemenino.findById(id);
          //   const clubMasculino = await ClubMasculino.findById(id);
          //let historialString = historialExiste.pedido[0].id.toString();
          //console.log('Este es historialString: ' + historialString);
          // console.log(
          //   'Este es el tipo de historialString: ' + typeof historialString
          // );
          // console.log(
          //   'Este es historialExiste.pedido[0].id: ' +
          //     historialExiste.pedido[0].id
          // );
          // console.log(
          //   'Este es el tipo de historialExiste.pedido[0].id: ' +
          //     typeof historialExiste.pedido[0].id
          // );
          // console.log('Este es taller: ' + taller);
          // console.log('Este es tipo de taller: ' + typeof taller);

          // if (taller) {
          //   if (historialString == taller._id) {
          //     console.log('id de historialExiste igual al de taller');
          //     throw new Error(`El Taller: ${taller.nombre} ya fue tomado`);
          //   }
          // }
          // if (club) {
          //   if (historialString == club._id) {
          //     console.log('id de historialExiste igual al de Club');
          //     throw new Error(`El Club: ${club.nombre} ya fue tomado`);
          //   }
          // }
          // if (clubFemenino) {
          //   if (historialString == clubFemenino._id) {
          //     console.log('id de historialExiste igual al de Club Femenino');
          //     throw new Error(
          //       `El Club: ${clubFemenino.nombre._id} ya fue tomado`
          //     );
          //   }
          // }
          // if (clubMasculino) {
          //   if (historialString == clubMasculino._id) {
          //     console.log('id de historialExiste igual al de Club Masculino');
          //     throw new Error(
          //       `El Club: ${clubMasculino.nombre} ya fue tomado`
          //     );
          //   }
          // }
          //   console.log(
          //     'Esto es historialExiste ya al final',
          //     historialExiste.pedido[0]
          //   );
          //   historialExiste.pedido.push({ id: id, cantidad: cantidad });
          //   historialExiste.total = historialExiste.total + 1;
          //   await historialExiste.save();
          //   const resultado = await historialExiste.save();
          //   return resultado;
          // }
          // console.log('Estoy llegando al último punto o no de agregar?????');
          // historialExiste.push({
          //   'pedido.id': input.pedido.id,
          //   'pedido.cantidad': 1,
          // });
          // historialExiste.total = historialExiste.total + 1;
          // // await Historial.save();
          // const resultado = await Historial.save();
          // return resultado;
        }
      }
    },
    actualizarHistorial: async (_, { id, input }) => {
      const { estudiante } = input;

      //Verificar si estudiante existe o no
      let estudianteExiste = await Estudiante.findById(estudiante);

      if (!estudianteExiste) {
        throw new Error('Ese estudiante no existe');
      }

      //Revisar que haya cupo disponible

      let historialExiste = await Historial.findOne({
        estudiante,
      });

      console.log('Esto es historial existe' + historialExiste);

      //Revisar el cupo
      if (input.pedido) {
        for await (const articulo of input.pedido) {
          const { id } = articulo;

          const taller = await Taller.findById(id);
          const club = await Club.findById(id);
          const clubFemenino = await ClubFemenino.findById(id);
          const clubMasculino = await ClubMasculino.findById(id);

          console.log('Esto es taller existe' + taller);
          if (taller) {
            // Restar la cantidad a lo disponible
            taller.existencia = taller.existencia - articulo.cantidad;

            await taller.save();
          }
          if (club) {
            if (articulo.cantidad > club.existencia) {
              throw new Error(
                `El Club: ${club.nombre} excede el cupo disponible`
              );
            } else {
              // Restar la cantidad a lo disponible
              club.existencia = club.existencia - articulo.cantidad;

              await club.save();
            }
          }
          if (clubFemenino) {
            if (articulo.cantidad > clubFemenino.existencia) {
              throw new Error(
                `El Club: ${clubFemenino.nombre} excede el cupo disponible`
              );
            } else {
              // Restar la cantidad a lo disponible
              clubFemenino.existencia =
                clubFemenino.existencia - articulo.cantidad;

              await clubFemenino.save();
            }
          }
          if (clubMasculino) {
            if (articulo.cantidad > clubMasculino.existencia) {
              throw new Error(
                `El Club: ${clubMasculino.nombre} excede el cupo disponible`
              );
            } else {
              // Restar la cantidad a lo disponible
              clubMasculino.existencia =
                clubMasculino.existencia - articulo.cantidad;

              await clubMasculino.save();
            }
          }
        }
      }

      // const taller = await Taller.findById(id);
      // const club = await Club.findById(id);
      // const clubFemenino = await ClubFemenino.findById(id);
      // const clubMasculino = await ClubMasculino.findById(id);

      // console.log('Esto es taller', taller);
      // if (taller) {
      //   // Sumar la cantidad a lo disponible
      //   taller.existencia = taller.existencia + 1;

      //   await taller.save();
      // }
      // if (club) {
      //   // Sumar la cantidad a lo disponible
      //   club.existencia = club.existencia + 1;

      //   await club.save();
      // }
      // if (clubFemenino) {
      //   // Sumar la cantidad a lo disponible
      //   clubFemenino.existencia = clubFemenino.existencia + 1;

      //   await clubFemenino.save();
      // }
      // if (clubMasculino) {
      //   // Sumar la cantidad a lo disponible
      //   clubMasculino.existencia = clubMasculino.existencia + 1;

      //   await clubMasculino.save();
      // }

      //Guardar el historial
      const resultado = await Historial.findOneAndUpdate(
        { estudiante: input.estudiante },
        input,
        {
          new: true,
        }
      );
      return resultado;

      // if (historialExiste.length == 2) {
      //   throw new Error('Talleres y Clubs concluidos');
      // }

      // if (historialExiste.length == 1) {
      //   console.log(
      //     'Esta entrando la condicion de historial agregado o no???'
      //   );
      //   const nuevoHistorial = new Historial(input);

      //   //Asignarle un grupo
      //   nuevoHistorial.grupo = ctx.usuario.id;

      //   const resultado = await nuevoHistorial.save();
      //   return resultado;
      // }
      // }
    },
    // nuevoHistorial: async (_, { input }) => {
    //   const { estudiante, tallerClub, contador } = input;

    //   //Verificar si estudiante existe o no
    //   let historialExiste = await Historial.find({})
    //     .populate('Estudiante')
    //     .findOne({ estudiante });

    //   if (historialExiste) {
    //     let tallerExiste = await Historial.find({})
    //       .populate('Taller')
    //       .findOne({ tallerClub });
    //     let clubExiste = await Historial.find({})
    //       .populate('Club')
    //       .findOne({ tallerClub });
    //     let clubMasculinoExiste = await Historial.find({})
    //       .populate('Taller')
    //       .findOne({ tallerClub });
    //     let clubFemeninoExiste = await Historial.find({})
    //       .populate('Taller')
    //       .findOne({ tallerClub });

    //     if (tallerExiste) {
    //       throw new Error('Este Taller ya fue cursado');
    //     }
    //     if (clubExiste) {
    //       throw new Error('Este Club ya fue cursado');
    //     }
    //     if (clubMasculinoExiste) {
    //       throw new Error('Este Club ya fue cursado');
    //     }
    //     if (clubFemeninoExiste) {
    //       throw new Error('Este Club ya fue cursado');
    //     }
    //     //Revisar que haya cupo disponible
    //     const contadorV = await historialExiste.contador;

    //     if (contadorV == 2) {
    //       throw new Error('Talleres y Clubes finalizados');
    //     } else {
    //       historialExiste.contador = contadorV + contador;
    //       await historialExiste.save();
    //     }
    //   } else {
    //     try {
    //       // Crear un nuevo pedido
    //       const nuevoHistorial = new Historial(input);

    //       //Asignarle un grupo
    //       //nuevoHistorial.grupo = ctx.usuario.id;

    //       //Guardarlo en la base de datos
    //       const resultado = await nuevoHistorial.save();
    //       return resultado;
    //     } catch (error) {
    //       console.log(error);
    //     }
    //   }
    // },
  },
};

module.exports = resolvers;
