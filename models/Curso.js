const mongoose = require('mongoose');

const CursosSchema = mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true,
  },
  existencia: {
    type: Number,
    required: true,
    trim: true,
  },
  fechaInicio: {
    type: Date,
    default: Date.now(),
    required: true,
  },
  fechaTermino: {
    type: Date,
    default: Date.now(),
    required: true,
  },
  horaInicio: {
    type: Date,
    default: Date.now,
    required: true,
  },
  horaTermino: {
    type: Date,
    default: Date.now,
    required: true,
  },
  creado: {
    type: Date,
    default: Date.now(),
    required: true,
  },
  maestro: {
    type: String,
    required: true,
    trim: true,
  },
  aula: {
    type: String,
    required: true,
    trim: true,
  },
});

module.exports = mongoose.model('Curso', CursosSchema);
