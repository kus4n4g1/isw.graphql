const mongoose = require('mongoose');

const Grupo2FSchema = mongoose.Schema({
  nombre: {
    type: String,
    trim: true,
    required: true,
  },
  paterno: {
    type: String,
    trim: true,
    required: true,
  },
  materno: {
    type: String,
    trim: true,
    required: true,
  },
  email: {
    type: String,
    trim: true,
    required: true,
  },
  nacimiento: {
    type: String,
    trim: true,
    required: true,
  },
  semestre: {
    type: String,
    trim: true,
    required: true,
  },
  genero: {
    type: String,
    trim: true,
    required: true,
  },
  curp: {
    type: String,
    trim: true,
    required: true,
  },
  sangre: {
    type: String,
    trim: true,
    required: true,
  },
  alergias: {
    type: String,
    trim: true,
    required: true,
  },
  padecimientos: {
    type: String,
    trim: true,
    required: true,
  },
  password: {
    type: String,
    trim: true,
    required: true,
  },
  creado: {
    type: Date,
    trim: true,
    default: Date.now(),
  },
});

module.exports = mongoose.model('Grupo2F', Grupo2FSchema);
