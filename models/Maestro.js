const mongoose = require('mongoose');

const MaestrosSchema = mongoose.Schema({
  nombre: {
    type: String,
    trim: true,
    required: true,
  },
  paterno: {
    type: String,
    trim: true,
    required: true,
  },
  materno: {
    type: String,
    trim: true,
    required: true,
  },
  email: {
    type: String,
    trim: true,
    required: true,
  },
  telefono: {
    type: String,
    trim: true,
  },
  celular: {
    type: String,
    trim: true,
    required: true,
  },
  curp: {
    type: String,
    trim: true,
    required: true,
  },
  sangre: {
    type: String,
    trim: true,
    required: true,
  },
  alergias: {
    type: String,
    trim: true,
    required: true,
  },
  padecimientos: {
    type: String,
    trim: true,
    required: true,
  },
  creado: {
    type: Date,
    trim: true,
    default: Date.now(),
  },
  grupo: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Usuario',
  },
});

MaestrosSchema.index({ paterno: 'text' });

module.exports = mongoose.model('Maestro', MaestrosSchema);
