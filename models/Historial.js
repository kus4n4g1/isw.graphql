const mongoose = require('mongoose');

const HistorialSchema = mongoose.Schema({
  pedido: {
    type: Array,
    required: true,
  },
  total: {
    type: Number,
    required: true,
  },
  estudiante: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Estudiante',
  },
  grupo: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Usuario',
  },
  estado: {
    type: String,
    default: 'PENDIENTE',
  },
  creado: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model('Historial', HistorialSchema);
