const mongoose = require('mongoose');

const ClubesSchema = mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true,
  },
  clave: {
    type: String,
    trim: true,
    required: true,
    unique: true,
  },
  existencia: {
    type: Number,
    required: true,
    trim: true,
  },
  dia: {
    type: String,
    required: true,
    trim: true,
  },
  fechaInicio: {
    type: Date,
    default: Date.now(),
    required: true,
  },
  fechaTermino: {
    type: Date,
    default: Date.now(),
    required: true,
  },
  horaInicio: {
    type: Date,
    default: Date.now,
    required: true,
  },
  horaTermino: {
    type: Date,
    default: Date.now,
    required: true,
  },
  creado: {
    type: Date,
    default: Date.now(),
    required: true,
  },
  maestro: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Maestro',
  },
  aula: {
    type: String,
    required: true,
    trim: true,
  },
});
ClubesSchema.index({ nombre: 'text' });

module.exports = mongoose.model('Club', ClubesSchema);
