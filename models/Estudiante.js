const mongoose = require('mongoose');

const EstudiantesSchema = mongoose.Schema({
  nombre: {
    type: String,
    trim: true,
    required: true,
  },
  paterno: {
    type: String,
    trim: true,
    required: true,
  },
  materno: {
    type: String,
    trim: true,
    required: true,
  },
  email: {
    type: String,
    trim: true,
    required: true,
  },
  nacimiento: {
    type: String,
    trim: true,
    required: true,
  },
  semestre: {
    type: String,
    trim: true,
    required: true,
  },
  genero: {
    type: String,
    trim: true,
    required: true,
  },
  curp: {
    type: String,
    trim: true,
    required: true,
  },
  sangre: {
    type: String,
    trim: true,
    required: true,
  },
  alergias: {
    type: String,
    trim: true,
    required: true,
  },
  padecimientos: {
    type: String,
    trim: true,
    required: true,
  },
  matricula: {
    type: String,
    trim: true,
    required: true,
    unique: true,
  },
  creado: {
    type: Date,
    trim: true,
    default: Date.now(),
  },
  grupo: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Usuario',
  },
});

EstudiantesSchema.index({ paterno: 'text' });

module.exports = mongoose.model('Estudiante', EstudiantesSchema);
